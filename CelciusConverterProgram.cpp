#include<iostream>
#include <math.h>
#include <algorithm>
#include <limits>
using namespace std;

void celsiusToFahrenheit();
void FahrenheitTocelsius();
void calcCircumference();
void calcCircleArea();
void calcRectangleArea();
void calcTriangleArea();
void calcCylinderVolume();
void calcConeVolume();
void inputValidation(int&, bool = false);

int main()
{
    int choice;
    int failState;
    cout << "1. Convert Celsius to Fahrenheit" << endl;
    cout << "2. Convert Fahrenheit to Celsius" << endl;
    cout << "3. Calculate Circumference of a circle" << endl;
    cout << "4. Calculate Area of a circle" << endl;
    cout << "5. Area of Rectangle" << endl;
    cout << "6. Area of Triangle(Heron's Formula)" << endl;
    cout << "7. Volume of Cylinder" << endl;
    cout << "8. Volume of Cone" << endl;
    cout << "9. Quit Program" << endl;

    do
    {
        failState = 0;
        cout << "\nEnter choice: ";
        cin >> choice;
        inputValidation(failState);
        if (failState == 0 && choice != 1 && choice != 2 && choice != 3 && choice != 4 && choice != 5 & choice != 6 & choice != 7 & choice != 8 & choice != 9)
        {
            cout << "\nChoice can only be 1, 2, 3, 4, 5, 6, 7, 8 or 9. Please try again.\n";
            failState = 1;
        }
    } while (failState == 1);
    switch (choice)
    {
    case 1:
    {
        celsiusToFahrenheit();
        break;
    }
    case 2:
    {
        FahrenheitTocelsius();
        break;
    }
    case 3:
    {
        calcCircumference();
        break;
    }
    case 4:
    {
        calcCircleArea();
        break;
    }
    case 5:
    {
        calcRectangleArea();
        break;
    }
    case 6:
    {
        calcTriangleArea();
        break;
    }
    case 7:
    {
        calcCylinderVolume();
        break;
    }
    case 8:
    {
        calcConeVolume();
        break;
    }
    case 9:
    {
        cout << "\nExiting system...\n";
        break;
    }
    default:
    {
        cout << "Invalid choice. Please try again.";
        break;
    }
    }

    return 0;
}


//start of function declaration

void celsiusToFahrenheit()
{
    float fahrenheit, celsius;
    int failState;

    do
    {
        failState = 0;
        cout << "Enter the Celsius Temperatures : ";
        cin >> celsius;
        inputValidation(failState);
    } while (failState == 1);

    fahrenheit = (celsius * 9) / 5 + 32;
    cout << "The Modified Temperature in Fahrenheit : " << fahrenheit << endl << endl;
}


void FahrenheitTocelsius()
{
    float fahrenheit, celsius;
    int failState;
    do
    {
        failState = 0;
        cout << "Enter the Fahrenheit Temperatures : ";
        cin >> fahrenheit;
        inputValidation(failState);
    } while (failState == 1);

    celsius = (fahrenheit - 32) * 5 / 9;
    cout << "The Modified Temperature in Celsius : " << celsius << endl << endl;
}

void calcCircumference()
{
    double radius, circumference;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the radius of the circle(cm): ";
        cin >> radius;
        inputValidation(failState);
        if (failState == 0 && radius <= 1)
        {
            failState = 1;
            cout << "Radius must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    circumference = radius * 2 * 3.1415;
    cout << "\nThe circumference of the circle is " << circumference << "cm." << endl;
}

void calcCircleArea()
{
    double radius, area;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the radius of the circle(cm): ";
        cin >> radius;
        inputValidation(failState);
        if (failState == 0 && radius <= 1)
        {
            failState = 1;
            cout << "Radius must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    area = 3.1415 * (radius * radius);
    cout << "\nThe area of the circle is " << area << "cm squared." << endl;
}

void calcRectangleArea()
{
    double L, W, area;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the width of the rectangle(cm): ";
        cin >> W;
        inputValidation(failState);
        if (failState == 0 && W <= 1)
        {
            failState = 1;
            cout << "\nWidth must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    do
    {
        failState = 0;
        cout << "\nPlease enter the length of the rectangle(cm): ";
        cin >> L;
        inputValidation(failState);
        if (failState == 0 && L <= 1)
        {
            failState = 1;
            cout << "\nLength must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    area = L * W;
    cout << "\nThe area of the rectangle is " << area << "cm squared." << endl;
}

void calcTriangleArea()
{
    double a, b, c, s, area;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the length of A side of the triangle (cm): ";
        cin >> a;
        inputValidation(failState);
        if (failState == 0 && a <= 1)
        {
            failState = 1;
            cout << "\nThe length of A side must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    do
    {
        failState = 0;
        cout << "\nPlease enter the length of B side of the triangle (cm): ";
        cin >> b;
        inputValidation(failState);
        if (failState == 0 && b <= 1)
        {
            failState = 1;
            cout << "\nThe length of B side must be more than 1. Please try again.\n";
        }
    } while (b <= 1);
    do
    {
        failState = 0;
        cout << "\nPlease enter the length of C side of the triangle (cm): ";
        cin >> c;
        inputValidation(failState);
        if (failState == 0 && c <= 1)
        {
            failState = 1;
            cout << "\nThe length of C side must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    s = (a + b + c) / 2;
    area = sqrt(s * (s - a) * (s - b) * (s - c));
    cout << "\nThe area of the triangle is " << area << "cm squared." << endl;
}

void calcCylinderVolume()
{
    double radius, height, volume;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the radius of the Cylinder(cm): ";
        cin >> radius;
        inputValidation(failState);
        if (failState == 0 && radius <= 1)
        {
            failState = 1;
            cout << "Radius value must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    do
    {
        failState = 0;
        cout << "\nPlease enter the height of the Cylinder(cm): ";
        cin >> height;
        inputValidation(failState);
        if (failState == 0 && height <= 1)
        {
            cout << "Height value must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    volume = 3.14159 * radius * radius * height;
    cout << "\nThe Volume of the Circle is " << volume << "cm." << endl;
}
void calcConeVolume()
{
    double radius, height, volume;
    int failState;
    do
    {
        failState = 0;
        cout << "\nPlease enter the radius of the Cylinder(cm): ";
        cin >> radius;
        inputValidation(failState);
        if (failState == 0 && radius <= 1)
        {
            failState = 1;
            cout << "Radius value must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    do
    {
        failState = 0;
        cout << "\nPlease enter the height of the Cylinder(cm): ";
        cin >> height;
        inputValidation(failState);
        if (failState == 0 && height <= 1)
        {
            cout << "Height value must be more than 1. Please try again.\n";
        }
    } while (failState == 1);
    volume = (3.14159 * radius * radius * height) / 3;
    cout << "\nThe Volume of the Cone is " << volume << "cm." << endl;
}

///function for input validation
void inputValidation(int& state, bool isChar)
{

    if (isChar)
    {
        char nextchar = getchar();
        while (nextchar != '\n')
        {
            if (!isspace(nextchar))
            {
                cout << "\nInput must be one character only! Please try again.\n";
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                state = 1;
                break;
            }
            nextchar = getchar();
        }
    }
    else
    {
        ///to handle wrong input format
        ///such as input non-numeric value for numeric input
        if (cin.fail())
        {
            ///return the bad state of input buffer to good state
            cout << "\nWrong input format, input should consists of number only. Please try again.\n";
            cin.clear();
            state = 1;
        }

        ///handling whitespaces for 'cin' or any other unwanted input left in input buffer
        ///Note: tried !cin, cin.bad, cin.fail, all not detecting whitespaces after a word such as a<space>a as error
        ///clear the unwanted input in the input buffer so that it won't affect next input
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
}
